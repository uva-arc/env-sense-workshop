/*
 * 
 * Big Dot v2 + Notecard Carrier B, v.1.0
 * Summer camp version
 *
 * Dependencies:
 * Big Dot Arduino core, v1.2.0
 * Blues Wireless library, v.1.4.1
 * PMserial, v.1.2.0
 * ArduinoLowPower, modified
 *
 * Copyright 2022 Blues Inc. All rights reserved.
 * Copyright 2024 unixjazz. All rights reversed.
 *
 * See LICENSE file for details.
 *
 */
#include <Wire.h>
#include <PMserial.h>
#include <Notecard.h>
#include <ArduinoLowPower.h>

// Enable DEBUG
#define DEBUG

// SerialUSB output
#define usbSerial Serial

// PM sensor
#define USE_HWSERIAL1

SerialPM pms(PMSx003, Serial1);
int pmsRSTpin = D2;
int pm_read_elapsed = 0;
int error_code_pm = 999;

// Notecard dev ID
#define PRODUCT_UID "ADD_PROD_UID_HERE"
#define myProductID PRODUCT_UID

// Notecard obj
Notecard notecard;

// Sync duration
unsigned long start_sync_time = 0;
unsigned long end_sync_time = 0;
unsigned long total_sync_time = 0;
unsigned int sync_delay = 3;

void setup()
{
  #ifdef DEBUG
    usbSerial.begin(115200);
    const size_t usb_timeout_ms = 3000;
    for (const size_t start_ms = millis(); !usbSerial && (millis() - start_ms) < usb_timeout_ms;)
      ;
    notecard.setDebugOutputStream(usbSerial);
    Serial.println("**********************************************");
    Serial.println("**** PM sensorbox v1.0: Hello North Slope ****");
    Serial.println("**********************************************");
    pinMode(LED_BUILTIN, OUTPUT);
  #endif

  // Init PM sensor
  pms.init();
  pinMode(pmsRSTpin, OUTPUT);
  digitalWrite(pmsRSTpin, HIGH);

  // Init Notecard
  notecard.begin();

  // Init Notecard auth
  J *req_set = notecard.newRequest("hub.set");
  JAddStringToObject(req_set, "product", myProductID);
  JAddStringToObject(req_set, "mode", "minimum");
  notecard.sendRequestWithRetry(req_set, 5);
  #ifdef DEBUG
    Serial.println("Notecard: hub.set");
  #endif

  // Disable Notecard accelerometer
  J *req_accel = notecard.newRequest("card.motion.mode");
  JAddBoolToObject(req_accel, "stop", true);
  notecard.sendRequestWithRetry(req_accel, 5);
  #ifdef DEBUG
    Serial.println("Notecard: disabled accelerometer");
  #endif
  
  // Disable Notecard GPS
  J *req_gps = notecard.newRequest("card.location.mode");
  JAddBoolToObject(req_gps, "off", true);
  notecard.sendRequestWithRetry(req_gps, 5);
  #ifdef DEBUG
    Serial.println("Notecard: disabled GPS");
  #endif
}

void loop()
{    
  // Start counting sync time
  start_sync_time = millis();
  #ifdef DEBUG
    Serial.println("");
    Serial.println(":: Start calculating time to sync ::");
    Serial.print("SYNC: ");
    Serial.print(start_sync_time / 1000);
    Serial.println(" seconds");
  #endif

  // PMS read
  #ifdef DEBUG
    Serial.println("");
    Serial.println(":: Start sensor readings ::");
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println("PMS: Request read");
    Serial.println("PMS: Stabilizing for 30sec");
  #endif
  digitalWrite(pmsRSTpin, HIGH);
  pms.wake();
  pms.read();
  delay(30 * 1000);

  // Requests and validates PM sensor data
  // Retries for valid data, before continuing
  pm_read_elapsed = 0;
  for (int counter = 0; counter < 60; counter++)
  {
    pms.read();
    if (pms.has_particulate_matter()) {
      if (pms.pm25 == 0 && pms.pm10 == 0) {
        #ifdef DEBUG
          Serial.print("PMS: Zero values found, re-trying, step #");
          Serial.println(counter);
        #endif     
        delay(1000);
        pm_read_elapsed += 1;
        continue;
      }
      #ifdef DEBUG
        Serial.print("PMS: PM2.5 reading: ");
        Serial.println(pms.pm25);
        Serial.print("PMS: PM10 reading: ");
        Serial.println(pms.pm10);
        Serial.print("");
      #endif
      break;
    }
    else {
      delay(1000);
      pm_read_elapsed += 1;
      pms.pm25 = error_code_pm;
      pms.pm10 = error_code_pm;
      #ifdef DEBUG
        Serial.print("PMS: PM 2.5 error, registered error code ");
        Serial.println(error_code_pm);
        Serial.print("PMS: PM 10 error, registered error code ");
        Serial.println(error_code_pm);
        Serial.println("");
        switch (pms.status) {
          case pms.ERROR_TIMEOUT:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_TIMEOUT));
            break;
          case pms.ERROR_MSG_UNKNOWN:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_MSG_UNKNOWN));
            break;
          case pms.ERROR_MSG_HEADER:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_MSG_HEADER));
            break;
          case pms.ERROR_MSG_BODY:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_MSG_BODY));
            break;
          case pms.ERROR_MSG_START:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_MSG_START));
            break;
          case pms.ERROR_MSG_LENGTH:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_MSG_LENGTH));
            break;
          case pms.ERROR_MSG_CKSUM:
            Serial.print("PMS: ");
            Serial.println(F(PMS_ERROR_MSG_CKSUM));
            break;
        }
      #endif
      continue;
    }
  }
  // PMS turn-off
  pms.sleep();
  digitalWrite(pmsRSTpin, LOW);

  // Retrieve Notecard temp and voltage
  double temperature = 0;
  J *rsp_temp = notecard.requestAndResponse(notecard.newRequest("card.temp"));
  if (rsp_temp != NULL)
  {
    temperature = JGetNumber(rsp_temp, "value");
    notecard.deleteResponse(rsp_temp);
  }

  double voltage = 0;
  J *rsp_volt = notecard.requestAndResponse(notecard.newRequest("card.voltage"));
  if (rsp_volt != NULL)
  {
    voltage = JGetNumber(rsp_volt, "value");
    notecard.deleteResponse(rsp_volt);
  }
  
  // Notecard: note.add with sensor data
  J *req_note = notecard.newRequest("note.add");
  if (req_note != NULL)
  {
    JAddBoolToObject(req_note, "sync", true);
    J *body = JAddObjectToObject(req_note, "body");
    if (body != NULL)
    {
      JAddNumberToObject(body, "pm02_5", pms.pm25);
      JAddNumberToObject(body, "pm10_0", pms.pm10);
      JAddNumberToObject(body, "temperature", temperature);
      JAddNumberToObject(body, "voltage", voltage);
    }
    notecard.sendRequest(req_note);
    #ifdef DEBUG
      Serial.println("Notecard: Submitted data to note");
    #endif
  }

  // Sync with Notehub
  J *req_sync = notecard.newRequest("hub.sync"); 
  notecard.sendRequest(req_sync);
  #ifdef DEBUG
    Serial.println("Notecard: hub.sync");
  #endif
  
  // Calculate sync time
  end_sync_time = millis();
  total_sync_time = end_sync_time - start_sync_time;
  #ifdef DEBUG
    Serial.println("");
    Serial.print("SYNC: Start sync time in seconds: ");
    Serial.println(start_sync_time / 1000);
    Serial.print("SYNC: End sync time in seconds: ");
    Serial.println(end_sync_time / 1000);
    Serial.print("SYNC: Total sync time in seconds: ");
    Serial.println(total_sync_time / 1000);
    Serial.print("PM read elapsed in seconds: ");
    Serial.println(pm_read_elapsed);
    Serial.println("");
  #endif

  // Standby mode: system sleep
  #ifdef DEBUG
    Serial.println("");
    Serial.print("PMS: Re-read requests: ");
    Serial.println(pm_read_elapsed);
    Serial.println("Big Dot: Sleeping...");
    digitalWrite(LED_BUILTIN, LOW);
    //
    // Sleep interval settings:
    // 5min testing loop - variable sync time - "sync magic number":
    int sleep_time = ((300 * 1000) - (total_sync_time) - (sync_delay * 1000));
    LowPower.sleep(sleep_time);
  #else
    //
    // 1h (sleeptime) - variable sync time - "sync magic number":
    int sleep_time = ((3600 * 1000) - (total_sync_time) - (sync_delay * 1000));
    LowPower.sleep(sleep_time);
  #endif
}
