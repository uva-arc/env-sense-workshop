# UVA-ARC: Environmental Sensing Workshop 

In this repository we keep all the learning resources on the basics of environmental sensing for the NSF UVA-ARC project.

## PM sensor

We are extending the sensor network with Open Hardware-based particulate matter sensors. Our system includes:

- Big Dot microcontroller board v2
- Notecard Carrier B
- PMS7003 particulate matter sensor
